代码规范：

常量变量
 1.全局常量和类常量的命名采用字母全部大写，单词之间加下画线的方式。
 2.采用驼峰式命名

空格
 2.在在运算符、赋值、参数等之间很少使用空格来隔开各种元素之间的距离

控制语句
一般在 if, for, while, do-while 等语句中必须使用大括号，即使只有一行代码，也应该加上大括号

例如：
int sum = 0;
int bigBaby；
for(int i = 0; i < 10; i++) {
       sum += i;
}
大括号
个人一般用
while
{
//******
}